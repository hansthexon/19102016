using UnityEngine;
using System.Collections;

public class Plane_anim : MonoBehaviour
{
    public GameObject planeButton;


    Animator planeAnim;
	
    // Use this for initialization
    void Start()
    {
        planeAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void planeAnimate()
    {
        planeAnim.SetTrigger("PlaneClick_Trigger");

    }
}
