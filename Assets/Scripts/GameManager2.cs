﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager2 : MonoBehaviour {

	public GameObject StarBracket;
	public GameObject PlanetBracket;
	int currentscore;
	public Text CurrentScoretext;
	public	Text StarScoreText;
	public Text PlanerScoreText;
	string StarScore="StarScore";
	string PlanetScore="PlanetScore";

	public	bool cantrack=false;

	bool politeplanet=true;
//	public GameObject MainCam;
//	public GameObject ArCam;
	public GameObject videoManager;
	Code codeobject;
	//public GameObject videoprefab;
	public MediaPlayerCtrl player;
	public	string tempfilename;
	public RawImage videoscreen;
	public 	GameObject mainpanel;

	public GameObject Imagsets;

	public string filepath;
	DataService ds;
	public GameObject Scorepanel;


	int i=0;
	// Use this for initialization
	void Start () {
		ds = new DataService ("arapp.sqlite");

		tempfilename= "quora";

		CurrentScoretext = CurrentScoretext.GetComponent<Text> ();
		StarScoreText = StarScoreText.GetComponent<Text> ();
		PlanerScoreText = PlanerScoreText.GetComponent<Text> ();
	
		StarScoreText.text=PlayerPrefs.GetInt(StarScore).ToString();
		PlanerScoreText.text=PlayerPrefs.GetInt(PlanetScore).ToString();

		player = videoManager.GetComponent<MediaPlayerCtrl> ();


		//	player=videoManager.GetComponent<MediaPlayerCtrl> ();
		//	player.OnEnd += SetScore;
	}

	// Update is called once per frame
	void Update () {

	}



	public void OpenArCam(){
		cantrack = true;
		politeplanet = true;
		//MainCam.SetActive (false);
	//	ArCam.SetActive (true);
		Imagsets.SetActive (true);
		mainpanel.SetActive (false);

		StarBracket.SetActive (false);
		PlanetBracket.SetActive (false);


	}
	// called by imagetargetclass and it will provide the name of the file found
	public void ObjectFound(string filename){

		politeplanet = true;

		fetchInfoFromDb (filename);
		//fetch all records related tothis clip
		//check if file has been purchased

		Debug.Log (codeobject.Purchased);
		if (codeobject.Purchased == "Y") {
			if (codeobject.Location == "I") {

				filepath = filename + ".mp4";
			} else {
				filepath="file://"+Application.persistentDataPath+"/"+ filename + ".mp4";

			}

			player.Load (filepath);

			player.OnEnd += SetScore;
			//
	

			//MainCam.SetActive (true);
			//	ArCam.SetActive(false);


			//Imagsets.SetActive (false);   // imagesets


			//
		
		}

	}

void fetchInfoFromDb(string filename){

		codeobject=ds.getVideo(filename);


	}

	void SetScore(){


		//videoscreen.gameObject.SetActive (false);
		player.OnEnd -= SetScore;
		//Destroy (videoManager);
		//not this one
	
		Scorepanel.SetActive (true);
		SaveScore (true);
	


	}


	void SetPlanetScore(){
		//update score
		politeplanet=false;
		//videoscreen.gameObject.SetActive (false);
		player.OnEnd -= SetPlanetScore;
		//videoManager.SetActive (false);
	//	Destroy (videoManager);

		Scorepanel.SetActive (true);
		SaveScore (false);


	}

	public 	void Setmyscore(){
		//videoscreen.gameObject.SetActive (false);



		player.OnEnd -= SetScore;
		Destroy (videoManager);


		Scorepanel.SetActive (true);
		//mainpanel.SetActive (true);


	}




	public void backButton(){

		if(codeobject.PolitePlanet=="Y"&&politeplanet){
			///if we keep files not in streaming assets
			/// if internal
			/// 
			/// 

			Scorepanel.SetActive (false);
			if (codeobject.Location == "I") {
				filepath = codeobject.SelfVideoFile + ".mp4";

			} else {


				filepath="file://"+Application.persistentDataPath+"/"+codeobject.SelfVideoFile+".mp4";

			}

	
			player.Load (filepath);
			player.Play ();


			player.OnEnd += SetPlanetScore;



		}else{


			videoManager.SetActive (false);
			Scorepanel.SetActive (false);
			mainpanel.SetActive (true);



			//cantrack = true;

		}


	}


	public void Replay(){
		Scorepanel.SetActive (false);
		//videoManager.SetActive (false);
		if (politeplanet) {

		//	videoManager.GetComponent<MediaPlayerCtrl> ().Play();

			//playvideo ();

			player.Play ();
			player.OnEnd += SetScore;


		} else {


			//playvideo ();
			player.Play ();
			player.OnEnd += SetPlanetScore;

		}
	}
	 void playvideo(){
		StarBracket.SetActive (false);
		PlanetBracket. SetActive (false);
		videoscreen.gameObject.SetActive (true);
		//player.Load (filepath);
		player.Play ();
		videoManager.SetActive (true);
	//	videoManager =	Instantiate (videoprefab) as GameObject;

		//player = videoManager.GetComponent<MediaPlayerCtrl> ();

	}



	bool isSelfFirst(){
		
		return codeobject.SelfFirst == "Y";
		}


	bool isFirst(){

		return codeobject.First == "Y";
	}


	void SaveScore(bool isStar){


		PlanetBracket.SetActive (true);
		StarBracket.SetActive (true);
		
		if (isStar) {
			if (codeobject.First == "Y") {
				
				PlayerPrefs.SetInt (StarScore,PlayerPrefs.GetInt (StarScore)+ codeobject.FirstScore);
				currentscore = codeobject.FirstScore;
				codeobject.First = "N";
				ds.UpdateFirst (codeobject);

			} else {
				PlayerPrefs.SetInt (StarScore,PlayerPrefs.GetInt (StarScore)+codeobject.RescanScore);
				currentscore	= codeobject.RescanScore;
               }


		} else {

			if (codeobject.SelfFirst == "Y") {

				PlayerPrefs.SetInt (PlanetScore,PlayerPrefs.GetInt (PlanetScore)+codeobject.SelfFirstScore);
				currentscore = codeobject.SelfFirstScore;
				codeobject.SelfFirst = "N";
				ds.UpdateFirst (codeobject);

			} else {
				PlayerPrefs.SetInt (PlanetScore,PlayerPrefs.GetInt (PlanetScore)+codeobject.SelfRescanScore);

				currentscore = codeobject.SelfRescanScore;



			}

		}
	

		UpdateDisplay ();



	}


	void UpdateDisplay(){
		
		StarScoreText.text=PlayerPrefs.GetInt (StarScore).ToString();
		PlanerScoreText.text=	PlayerPrefs.GetInt (PlanetScore).ToString();
		CurrentScoretext.text = currentscore.ToString ();


	}

}
