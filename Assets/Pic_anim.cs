using UnityEngine;
using System.Collections;

public class Pic_anim : MonoBehaviour
{
    public GameObject picButton;

    Animator picAnim;
    // Use this for initialization
    void Start()
    {
        picAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void picAnimate()
    {
        picAnim.SetTrigger("PicClick_Trigger");
    }
}
