﻿using UnityEngine;
using System.Collections;

public class Doll_anim : MonoBehaviour
{
    public GameObject dollButton;

    Animator dollAnim;
    // Use this for initialization
    void Start()
    {
        dollAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void dollAnimate()
    {
        dollAnim.SetTrigger("DollClick_Trigger");
    }
}
