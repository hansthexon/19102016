using UnityEngine;
using System.Collections;

public class Warmmup_anim : MonoBehaviour
{
    public GameObject warmmupButton;

    Animator warmmupAnim;
    // Use this for initialization
    void Start()
    {
        warmmupAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void warmmupAnimate()
    {
        warmmupAnim.SetTrigger("Warmup_Trigger");
    }
}
